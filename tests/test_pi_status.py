#!/usr/bin/env python3

import unittest
import os
import sys
import time
import ipaddress

parent_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(parent_path)

from unittest.mock import patch
from pi_status import get_args, get_creds, get_ps_output, localTime, get_top_N_processes, get_process_info

class TestPiStatus(unittest.TestCase):

    def test010_get_args(self):
        '''
        Tests the -n and --num of options from get_args function.
        '''
        CMDLINE = ['-i', '192.168.10.11', '-t', 'CPU', '-n', '12']
        type_1, num, ip = get_args(CMDLINE=CMDLINE)
        self.assertIsInstance(num, int, msg='Num is not a integer')

        CMDLINE = ['-i', '192.168.10.11', '-t', 'CPU', '--num', '12']
        type_1, num, ip = get_args(CMDLINE=CMDLINE)
        self.assertIsInstance(num, int, msg='Num is not a integer')

    def test020_get_args(self):
        '''
        Tests the -i and --ip-address options from get_args function.
        '''
        CMDLINE = ['-i', '192.168.10.1i1', '-t', 'CPU', '-n', '12']
        type_1, num, ip = get_args(CMDLINE=CMDLINE)
        with self.assertRaises(ValueError, msg='Not Valid IP address'):
            ipaddress.ip_address(ip)

        CMDLINE = ['--ip-address', '192.168.10.1i1', '-t', 'CPU', '-n', '12']
        type_1, num, ip = get_args(CMDLINE=CMDLINE)
        with self.assertRaises(ValueError, msg='Not Valid IP address'):
            ipaddress.ip_address(ip)

    def test030_get_args(self):
        '''
        Tests -t and --type options from get_args function
        '''
        CMDLINE = ['-i', '192.168.10.11', '-t', 'CPU', '-n', '12']
        type_1, num, ip = get_args(CMDLINE=CMDLINE)
        self.assertEqual(type_1.upper(), 'CPU', msg='Not valid type option')

        CMDLINE = ['-i', '192.168.10.11', '--type', 'MEMORY', '-n', '12']
        type_1, num, ip = get_args(CMDLINE=CMDLINE)
        self.assertEqual(type_1.upper(), 'MEMORY', msg='Not valid type option')


    @patch('getpass.getpass')
    def test040_get_creds(self, getpass):
        '''
        Tests the get_creds function.
        '''
        getpass.return_value = 'test'
        self.assertEqual(get_creds(), 'test', msg='Passwords do not match')
    
    @patch('netmiko.ConnectHandler')
    def test050_get_ps_output(self, netmiko_call):
        '''
        Test the get_ps_output function. It mocks the netmiko.ConnectHandler.send_command() call. 
        TODO: Break appart this function so that the netmiko call and the str to list conversion 
        are in different functions. 
        '''
        netmiko_call.return_value = '''USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
        root         1  0.0  0.0 239372 12488 ?        Ss   15:30   0:08 /usr/lib/systemd/systemd --switched-root --system --deserialize 32
        root         2  0.0  0.0      0     0 ?        S    15:30   0:00 [kthreadd]
        root         3  0.0  0.0      0     0 ?        I<   15:30   0:00 [rcu_gp]'''

        netmiko_call.send_command = lambda x: str(netmiko_call.return_value)
        output = get_ps_output(netmiko_call)
        
        test_list = [
                    ['USER', 'PID', '%CPU', '%MEM', 'VSZ', 'RSS', 'TTY', 'STAT', 'START', 'TIME', 'COMMAND'],
                    ['root', '1', '0.0', '0.0', '239372', '12488', '?', 'Ss', '15:30', '0:08', '/usr/lib/systemd/systemd', '--switched-root', '--system', '--deserialize', '32'],
                    ['root', '2', '0.0', '0.0', '0', '0', '?', 'S',  '15:30', '0:00', '[kthreadd]'],
                    ['root', '3', '0.0', '0.0', '0', '0', '?', 'I<', '15:30', '0:00', '[rcu_gp]']
                    ]
                    
        self.assertEqual(output, test_list, msg='Lists do not match')

    @patch('netmiko.ConnectHandler')
    def test060_get_top_N_processes(self, netmiko_call):
        '''
        Tests get_top_N_processes and ensures a list of PID is returned. 
        '''
        netmiko_call.return_value = '''USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
        root         1  0.0  0.0 239372 12488 ?        Ss   15:30   0:08 /usr/lib/systemd/systemd --switched-root --system --deserialize 32
        root         2  0.0  1.0      0     0 ?        S    15:30   0:00 [kthreadd]
        root         3  0.0  0.0      0     0 ?        I<   15:30   0:00 [rcu_gp]'''

        netmiko_call.send_command = lambda x: str(netmiko_call.return_value)
        pid_str = get_top_N_processes(netmiko_call, num_of_processes=5, TYPE='CPU')
        
        self.assertEqual(pid_str, '1 2 3', msg='PID\'s are not what was expected')
        
        
    @patch('netmiko.ConnectHandler')
    def test070_get_process_info(self, netmiko_call):
        '''
        Tests get_process_info function. 
        '''
        netmiko_call.return_value = '''USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
        root         1  0.0  0.6   5444  3296 ?        Ss   Jun02   5:03 /sbin/init spla
        root         2  0.0  0.0      0     0 ?        S    Jun02   0:00 [kthreadd]
        root         3  0.0  0.0      0     0 ?        S    Jun02   8:24 [ksoftirqd/0]
        root         5  0.0  0.0      0     0 ?        S<   Jun02   0:00 [kworker/0:0H]
        root         7  0.0  0.0      0     0 ?        S<   Jun02   0:00 [lru-add-drain]
        mysql     1301  0.4  0.7 318060  3728 ?        Sl   Jun02 399:34 /usr/sbin/mysql
        root     27175  6.6  1.0  12140  5212 ?        Ss   00:50   0:00 sshd: pi [priv]
        pi       27180  1.7  0.6   4964  3144 ?        Ss   00:50   0:00 /lib/systemd/sy
        pi       27186  2.4  0.8  12140  3996 ?        S    00:50   0:00 sshd: pi@pts/0
        pi       27188 18.1  0.9   6500  4456 pts/0    Ss   00:50   0:01 -bash
        '''.strip()

        netmiko_call.send_command = lambda x: str(netmiko_call.return_value)
        output = get_process_info(netmiko_call, pid_str='27188 27175 27186 27180 1301 1 2 3 5 7')

        compare_list =  [
                        'USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND',
                        'pi       27188 18.1  0.9   6500  4456 pts/0    Ss   00:50   0:01 -bash',
                        'root     27175  6.6  1.0  12140  5212 ?        Ss   00:50   0:00 sshd: pi [priv]',
                        'pi       27186  2.4  0.8  12140  3996 ?        S    00:50   0:00 sshd: pi@pts/0',
                        'pi       27180  1.7  0.6   4964  3144 ?        Ss   00:50   0:00 /lib/systemd/sy',
                        'mysql     1301  0.4  0.7 318060  3728 ?        Sl   Jun02 399:34 /usr/sbin/mysql',
                        'root         1  0.0  0.6   5444  3296 ?        Ss   Jun02   5:03 /sbin/init spla',
                        'root         2  0.0  0.0      0     0 ?        S    Jun02   0:00 [kthreadd]',
                        'root         3  0.0  0.0      0     0 ?        S    Jun02   8:24 [ksoftirqd/0]',
                        'root         5  0.0  0.0      0     0 ?        S<   Jun02   0:00 [kworker/0:0H]',
                        'root         7  0.0  0.0      0     0 ?        S<   Jun02   0:00 [lru-add-drain]'
                        ]


        self.assertEqual(output, compare_list)




if __name__ == '__main__':
    unittest.main()

