#!/usr/bin/env python3
import getpass
import netmiko
import time
import sys
import argparse

def get_args(CMDLINE=None):

    about = 'Will login to the PI @ IP provided and print out the top CPU and Memory hogs.'
    parser = argparse.ArgumentParser(description=about)
    parser.add_argument(
                        '-i',
                        '--ip-address',
                        action='store',
                        required=True,
                        help='IP address to linux system'
                        )
    parser.add_argument(
                        '-t',
                        '--type', 
                        action='store',
                        help='CPU or MEMORY - Displays the top hogs for each type.'
                        )
    parser.add_argument(
                        '-n',
                        '--num', 
                        action='store',
                        help='Top number of process based off of type',
                        )

    if CMDLINE == None:
        args = parser.parse_args()
    else:
        args = parser.parse_args(CMDLINE)

    return(args.type, int(args.num), args.ip_address)

def get_creds():
    '''
    Returns a Dict that has pi and users info.
    '''
    count = 0
    password1 = None
    password = getpass.getpass('Password: ')
    password1  =  getpass.getpass('Verify Password: ')
    while password1 != password:
        count += 1
        if count == 3:
            print ('Go home guy. You are drunk')
            sys.exit(1)
        print('Passwords do not match')
        password = getpass.getpass('Password: ')
        password1  =  getpass.getpass('Verify Password: ')


    return(password)

def get_ps_output(linux_device):

    output = linux_device.send_command('ps aux')
    output = output.splitlines()
    output = [ row.split() for row in output] 
    return (output)

def localTime():
    return(time.asctime(time.localtime(time.time())))


def get_top_N_processes(linux_device, num_of_processes=5, TYPE=''):
    '''
    Will retreive the PS output of the top N number of processes ordered by CPU 
    or MEMORY only. Default CPU.
    '''
    ps_output = get_ps_output(linux_device)
    headers = ps_output.pop(0)
    if 'CPU'.lower() in TYPE.lower():
        index = 2
    else:
        index = 3
    ps_sorted = sorted(ps_output, key=lambda row: float(row[index]), reverse=True)

    if len(ps_sorted) <= num_of_processes:
        num_of_processes = len(ps_sorted)

    hog_linux_processes = ''
    for i in range(num_of_processes):
        hog_linux_processes = ' ' + hog_linux_processes + ' ' + ps_sorted[i][1]
    
    return(hog_linux_processes.strip())


def get_process_info(linux_device, pid_str='1 2 3'):
    '''
    Returns all info ps output on proccesses in pid_list
    '''
    pid_linux_process = pid_str.strip()
    pids = linux_device.send_command('ps u {}'.format(pid_linux_process))
    pids = pids.splitlines()
    
    #Sometime on large PS output the top line is empty or has other values. 
    #This will check to make sure the PS header is at the top.
    processes = [pids.pop(0).strip()]
    while 'USER' not in processes[0]:
        processes =[pids.pop(0).strip()]

    for x in pid_linux_process.split(' '):
        for pid in pids:
            if pid.split()[1] == x:
                processes.append(pid.strip())
                break
    
    return (processes)

def main():
    '''
    Main program. Will get the top cpu and memory processes from ps.
    It wil then print out the results. 
    '''
    type1, num, ip = get_args()
    password = get_creds()
    connection = { 
            'username': 'pi',
            'password': password,
            'ip': ip,
            'device_type': 'linux'
            }
   
    with netmiko.ConnectHandler(**connection) as pi:
        print('1st ps call', localTime())
        pid_str = get_top_N_processes(pi, num_of_processes=num, TYPE=type1)
        print('2nd ps call', localTime())
        output = get_process_info(pi, pid_str=pid_str)
    print ('Disconnected')

    print ('\n'*5)
    for i in output:
        print (i)
    
if __name__ == '__main__':
    main()

