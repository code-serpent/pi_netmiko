FROM python:3.6
ADD . /opt
WORKDIR /opt
RUN pip install -r requirements.txt
#ENTRYPOINT ["/usr/local/bin/python3", "/opt/pi_status.py" ]
CMD ["/usr/local/bin/python3", "/opt/pi_status.py" ]

